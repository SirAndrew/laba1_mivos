package com.example.laba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button Convert1;
    private Button Convert2;
    private Button Convert3;
    private Button Convert4;
    private EditText decText;
    private String decStr;
    private TextView binText;
    private static final String KEY = "ANSWER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Convert1 = findViewById(R.id.dectobin);
        Convert1.setOnClickListener(this);
        Convert2 = findViewById(R.id.bintodec);
        Convert2.setOnClickListener(this);
        Convert3 = findViewById(R.id.dectohex);
        Convert3.setOnClickListener(this);
        Convert4 = findViewById(R.id.hextodec);
        Convert4.setOnClickListener(this);
        decText = findViewById(R.id.inputDec);
        binText =findViewById(R.id.outputBin);

        if (savedInstanceState != null) { //загрузка ответа при повороте экрана
            int answer = savedInstanceState.getInt(KEY);
            binText.setText(Integer.toString(answer));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) { //сохранение ответа при повороте экрана
        super.onSaveInstanceState(outState);
        try {
            outState.putInt(KEY, Integer.parseInt(binText.getText().toString()));
        }
        catch (NumberFormatException e){
            return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.dectobin:
                int tmp1 =-1;
                decStr = decText.getText().toString();
                if(decStr.length()>0 && decStr.length()<9){
                    try {
                        tmp1=Integer.parseInt(decStr);
                    }
                    catch (NumberFormatException e){
                        Toast toast = Toast.makeText(getApplicationContext(),"Ошибка ввода", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }

                if(tmp1>=0){
                    String bin = "";
                    while (tmp1!=0){
                        if(tmp1 % 2 == 0)
                            bin = "0"+ bin;
                        else
                            bin = "1" + bin;
                        tmp1 = tmp1/2;
                    }
                    binText.setText(bin);
                }
                break;
            case R.id.bintodec:
                int tmp2;
                decStr = decText.getText().toString();
                try {
                    tmp2=Integer.parseInt(decStr,2);
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Ошибка ввода", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                String output = Integer.toString(tmp2);
                binText.setText(output);
                break;
            case R.id.dectohex:
                int tmp3;
                decStr = decText.getText().toString();
                try {
                    tmp3=Integer.parseInt(decStr);
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Ошибка ввода", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                binText.setText(Integer.toHexString(tmp3).toUpperCase());

                break;

            case R.id.hextodec:
                int tmp4;
                decStr = decText.getText().toString();
                try {
                    tmp4=Integer.parseInt(decStr,16);
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Ошибка ввода", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                String output4 = Integer.toString(tmp4);
                binText.setText(output4);
                break;
        }
    }
}
